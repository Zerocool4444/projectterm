/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projecttarm;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author PlugPC
 */
public class Test {

    public static void main(String[] args) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        
        try {
            String sql = "SELECT id, username, password, tel, name, surname, address, idnumber, salary, type FROM user";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String username = result.getString("username");
                String password = result.getString("password");
                String tel = result.getString("tel");
                String name = result.getString("name");
                String surname = result.getString("surname");
                String address = result.getString("address");
                String idnumber = result.getString("idnumber");
                double salary = result.getDouble("salary");
                int type = result.getInt("type");
                User user = new User(id, username, password, tel, name, surname, address, idnumber, salary, type);
                System.out.println(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
