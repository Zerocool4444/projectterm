/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projecttarm;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author PlugPC
 */
public class TestCustomer {

    public static void main(String[] args) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();

        try {
            String sql = "INSERT INTO customer (name,surname,tel) VALUES (?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            Customer customer = new Customer(-1, "minimini", "plug1234", "0907704532",0);

            stmt.setString(1, customer.getName());
            stmt.setString(2, customer.getSurname());
            stmt.setString(3, customer.getTel());
            stmt.setInt(4, customer.getPoint());
            int row = stmt.executeUpdate();
            System.out.println("Affect row" + row);
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if (result.next()) {
                id = result.getInt(1);
            }
            System.out.println("Affect row" + row + " id: " + id);

        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
}
