/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;
import model.Customer;
import model.Product;
import model.BillAndProduct;
import model.User;

/**
 *
 * @author Zerocool
 */
public class Bill {
    private int id;
    private Date created;
    private User seller;
    private Customer customer;
    private double discount;
    private ArrayList<BillAndProduct> billAndProduct;

    public Bill(int id, Date created, User seller, Customer customer,double discount) {
        this.id = id;
        this.created = created;
        this.seller = seller;
        this.customer = customer;
        this.discount = discount;
        billAndProduct=new ArrayList<>();
    }

    public Bill(User seller, Customer customer,double discount) {
        this(-1,null,seller,customer,discount);
    }
    public Bill(User seller){
        this(-1,null,seller,null,0);
    }
          
    public void addRecieptDetail(int id,Product product,int amount,double price){
        for(int row=0;row<billAndProduct.size();row++){
            BillAndProduct r=billAndProduct.get(row);
            if(r.getProduct().getId()==product.getId()){
//                r.addAmount(amount);
                r.setAmount(amount);
                return;
            }            
        }
        billAndProduct.add(new BillAndProduct(id,product, amount,price, this));
    }
    public void addRecieptDetail(Product product,int amount ){
        addRecieptDetail(-1,product, amount,product.getPrice());
    }
    
    public void deleteBillAndProduct(int row){
        billAndProduct.remove(row);
    }
    
    public double getTotal(){
        double total=0;
        for(BillAndProduct r:billAndProduct){
            total=total+r.getTotal();
        }
        return (discount>0)?total-discount:total;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getDiscount() {
        return discount;
    }


    public ArrayList<BillAndProduct> getBillAndProduct() {
        return billAndProduct;
    }

    public void setBillAndProduct(ArrayList<BillAndProduct> billAndProduct) {
        this.billAndProduct = billAndProduct;
    }

    @Override
    public String toString() {
        String str= "Receipt{" + "id=" + id 
                + ", created=" + created 
                + ", seller=" + seller 
                + ", customer=" + customer 
                + ", discount=" + discount 
                + ", total=" + this.getTotal()
                +"}\n";
        for(BillAndProduct b:billAndProduct){
            str+=b.toString()+"\n";
        }
        return str;
    }
    
    
}